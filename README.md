# Rahm's Rice'd Desktop Environment

This is Rahm's Desktop Environment. It is a *very* configured XMonad/XMobar
based desktop environment.

[![Screenshot](screenshot.jpg)](https://git.josher.dev/cgit/rde.git/)

# Requirements

* GHC
* X11
* Xinerama
* Monofur NERD Font. Should be included with the NERD fonts package.
* GCC
* Perl
* Knowledge of Linux/Unix
* Linux, maybe Mac

# Configuration

## Window Manager

The Window Manager is a *heavily* configured XMonad. Many of the keys are
designed to be reminiscent of Vim.

Similar to Vim, Rde contains within it, its own kind of "language" called WML
(Window management language) to describe workspaces and windows to make very
powerful key bindngs. See the section about Window Management Language.

### Some things to note

I type in Programmer Dvorak, which means some of the key bindings make more
sense on the Dvorak layout vs QWERTY layout, but many of the keybindings should
work well in both.

The mod key is by default set to the Hyper Key. One would probably want to
change this in `Main.hs` before using this. Chose the hyper key to avoid conflicts
with other applications.

## Bar

The Bar is XMobar, pretty heavily configured and leverages the number of scripts
that ship with Rde.

### Features

  * OS Logo
    * For Debian, Arch, Ubuntu and BSD
  * Kernel version
  * Shows workspace layout as icon
  * Workspaces
    * Red is current
    * Blue is not current, but visible
    * Gray is not visible
  * Window Name
  * Time
  * CPU and Mem Usage displayed as dashes.
  * Weather Info
  * Spotify Info
  * Bluetooth Info
  * Battery Info
  * Date

## Battery

XMobar's battery plugin is not quite good enough. I want to be able to display
an icon as I'm charging, etc. So, I included a battery monitor C program
designed to interface with XMobar. Namely the program is in
`xmobar/extras/battery/battery.c`.

## Weather

The Weather is controlled by a barely functional Perl script which Hard Codes
the weather station to KLMO (Longmont, CO). Maybe you should change that? The
TODO here is to use wttr.in, but I'm too lazy to fix it.

## Spotify

If you don't use Spotify, then I'm sorry. It's one of the pieces of proprietary
garbage (PG, for short)  I allow on my system (along with the Nvidia Drivers and
Intel Management Sytem, ick).

Rde is admittedly coupled with Spotify, and contains a spotify-control
script to work with it. Maybe you should make that script work with your media
player?

## Bluetooth

The `bluetooth-select` script allows the user to connect to bluetooth devices.
If you haven't paired your bluetooth device yet, maybe you should use
`bluetoothctl`?

## Compositor

Picom/Compton is my compositor, and it mostly works, but sometimes doesn't.

## Window Management Language (WML)

There are some commands that operate on "Window Management Objects," (WMO) there
are two types of WMOs, Workspaces and Windows.

WML consists of "sentences," and sentences  and objects to operate on. For example: typing `<M-g>a`
will "goto" the workspace 'a'. Typing `<M-w>s` will go to the window tagged with
's'. Thats pretty simple, However things get more complex. For example:

  * `<M-s>\%@.,,^` Shift all windows not on the current workspace to the
    workspace on the third monitor from the left. 

  * `<M-s>\%*_` Kill all the Windows not currently visible

  * `<M-g>,.` Switch to the workspace to the right of the current workspace

  * `<M-s>,,~@.,.` Move all but the last two windows on the current workspace to
    the next monitor

  * `<M-s>|@a@b.` Move all the windows on workspaces 'a' and 'b' to the current
    workspace.

  * `<M-s>%.<M-s>%:s` Float all windows.

  * `<M-s>%.<M-s>%::s` Sink all windows.

  * `<M-s>--` Undo the last shift.

It looks complicated, but it's actually quite simple. All WML "sentances" start
have a "verb", which is usually triggered with a mod key. What comes next
depends to the verb.

### Verbs

  * `<M-g>[Workspace]` *goto*, jumps to a workspace
  * `<M-s>[WindowSet][Workspace]` *shift*, shifts windows to a workspace.
  * `<M-m>[WindowSet][:alpha:]` *mark*, marks a window with the given alpha
    character.
  * `<M-w>[WindowSet]` *navigate*, Navigates to the first window in the
    referenced windowset.

#### Workspace Object

Workspace object describe a place where windows can go. In XMonad workspaces can
either be hidden or visible. On a multi monitor setup multiple workspaces may be
visible at a single time.

The base of the Workspace object is the workspace tag. These are any alpha
numeric characters and are referenced by their tag. Thus the keystroke 'a'
references the 'a' workspace, 'b' references the 'b' workspace, 'π' references
thes 'π' workspace (yes, any alphanumeric character work).

So `<M-g>a` will simply go to the 'a' workspace.

That's pretty simple, but things get more interesting when using special
workspaces.

   * *x* | *x* is alphanumeric: the workspace with that tag 
   * `.`: The current workspace
   * `[`: The next non-visible workspace to the right of this one.
   * `]`: The next non-visible workspace to the left of this one.
   * `(`: The next workspace to the right
   * `)`: The next workspace to the left
   * `^`: The workspace on the rightmost screen.
   * `$`: The workspace on the leftmost screen.
   * `,`: The workspace on the monitor to the right of the next typed workspace
     (i.e. `,a` is the workspace to the right of workspace a. '
   * `;`: Like `,`, but to the left.
   * `:`: Delegates to the next workspace, but float windows sent to it. (i.e.
     `<M-s>@.:.` will float all the windows on the current screen.
   * `::`: Delegates to the next workspace, but sink windows sent to it. (i.e.
     `<M-s>@.::.` will sink all the windows on the current screen.
   * `/`: Search for a window and reference the workspace on that window.
   * `@`: The workspace associated with the following window set object. (i.e.
     `@a` references the workspace the window marked a is on)
   * ` ` (space): The current workspace's associated workspace. The associated
     workspace is the workspace with the tag of the toggled case (i.e.
     Associated workspace for 'a' is 'A' and vice versa). Note that workspaces
     with number tags do not have associated workspaces.
   * `_`: The black hole "workspace." Cannot be travelled to, but sending a
     window to it will kill it.
   * `-`: The alternate workspace. Each window has an alternate workspace, which
     is generally the last workspace that contained that window. Sending a
     window to the alternate workspace will send it to the last workspace it was
     a part of.
   * `*`: The hidden workspace. Acts just like other base workspaces except that
     it does not show up in the workspace list in XMobar and cannot be navigated
     to using `[` or `]` .

#### WindowSet Objects

Window set object reference sets of windows. Operations that only require one
Window will use the first element in the windowset referenced.

Windows can be marked using the `M-m` verb.

Like workspaces, the base are alpha (non-numeric), and these referenced marked
windows, but there are also special marks and operators to better describe
exactly what one is wanting to do.

   * *x* | x is alpha: The windows marked with *x*
   * `.`: The current window.
   * `^`: The window on the far left (across all screens)
   * `$`: The window on the far right (across all screens)
   * `'`: The last window
   * `*`: All currently visible windows
   * `-`: The windows used in the last command. (NOTE: <M-s>-- will send last
     referenced windows to where they came from -- basically undo)
   * `/`: Search for a window and select all the ones with that title
   * `%`: All windows
   * `_`: No windows
   * `@`: All the windows on the next entered workspace. (i.e. @,. references
     all windows on the creen to the right of the current workspace).
   * `!`: The first window in the following windowset (i.e. !@.
     references the master window on the current workspace).
   * `:`: The floating windows.
   * `,`: The following window set, but without the first element.
   * `~`: The following window set, but reversed (i.e. `!~@.` references the
     last window on the current workspace`
   * `?`: takes two window sets, if the first one is empty, then return the
     second one, otherwise return the first.
   * `|`: The union of the next two window sets (i.e. `|@.a` is the union of
     the windows on the current workspace and the windows marked with 'a'.
   * `&`: The intersection between the next two window sets.
   * `\`: The difference between two window sets. (i.e. `\%a` references all
     windows except those marked with 'a')

#### Revisiting Examples

  * `<M-s>\%@.,,^` *Shift all windows not on the current workspace to the
    workspace on the third monitor from the left.*

      * `<M-s>`: we're shifting windows to a workspace.
      * `\%@.`: All windows not on the current workspace (`\\` is the set
        difference operator, `%` is all windows, `@.` is all windows on the
        current workspace)
      * `,,^`: Third screen from the left. `^` references the far left screen,
        the `,` (applied twice) shifts that to the right once.

  * `<M-s>\%*_` *Kill all the Windows not currently visible*

      * `<M-s>` we're shifting windows.
      * `\%*` All windows except those visible.
      * `_` The black hole workspace. Kills windows sent to it.

  * `<M-s>,,~@.,.` *Move all but the last two windows on the current workspace to
    the next monitor*

      * `<M-s>`: We're shifting windows to a workspace
      * `,,~@.`: `@.` references the windows on the current workspace, `~`
        reverses that list, and `,` (applied twice) takes the first element off
        the list. So it's all windows on the current workspace, except the last
        two.
      * `,.`: The workspace on the screen to the right.

  * `<M-s>%.<M-s>%:-`: *Float all windows*

      * `<M-s>%.` Moves all the windows to the current workspace.
      * `<M-s>%:.` Moves all the windows back to where they came from, but
        floats them.

#### Some Identities

  * `<M-s>..` is a No-op. It move the current window to the current workspace,
    which does nothing.

  * `&x_` References nothing for any x because `_` is the empty windowset.

  * `|_x` References x for any x.

  * `<M-g>.` Is a No-op. It just goes to the current workspace

  * `<M-s>@xx` is a no-op for any x because it just moves all windows on
    workspace x to worksapce x.

  * `,;x` is just references x for any x because the `;` undos the `,`

#### Macros

Remembering and using some of these key sequences can be troublesome, especially
if that key sequences is used often. For this reason, RDE has the ability to
record macros to reference these objects.

To record a macro, type `<M-d>` then if
  * You want to record a windowset macro, type `w`
    1. Type the key chord to record the macro to (Ctrl+characters work well)
    1. Type the key sequence to record and hit `<Return>`
  * You want to record a workspace macro, type `t`
    1. Type the key chord to record the macro to (Ctrl+characters work well)
    1. Type the key sequence to record and hit `<Return>`

Example:

If one types `<M-d>w+\@..<Return>` this will record the macro `\@..` (Which
references all windows on the current workspace except the current window) as
`+`, so now one can type `<M-s>+_` to kill all the windows on the current
workspace except the current window.

NOTE: Recursive macros are not prohibited. Be careful!
