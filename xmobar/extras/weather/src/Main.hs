{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StrictData #-}

module Main where

import Control.Monad (when)
import Control.Monad.Writer hiding (getFirst)
import Data.Aeson
import Data.Aeson.Types (Parser)
import qualified Data.ByteString.Lazy.Char8
import qualified Data.Char
import Data.List (isInfixOf)
import qualified Data.Map
import Data.Maybe (fromMaybe)
import qualified Data.Text
import GHC.Generics
import Network.Curl
import System.Exit (exitFailure)
import System.IO (hPutStrLn, stderr)
import Text.Printf (printf)

-- Date time that easily comparable.
data SillyTime = SillyTime
  { sillyTimeDate :: !String,
    sillyTimeAMPM :: !String,
    sillyTimeTime :: !String
  }
  deriving (Ord, Eq, Show)

data CurrentCondition = CurrentCondition
  { feelsLikeF :: !String,
    cloudCover :: !String,
    humidity :: !String,
    tempF :: !String,
    weatherDesc :: !String,
    windspeedMiles :: !String,
    winddir :: !String,
    observationTime :: !SillyTime
  }
  deriving (Generic, Show)

newtype NearestArea = NearestArea
  { areaName :: String
  }
  deriving (Generic, Show)

data Astronomy = Astronomy
  { sunrise :: !SillyTime,
    sunset :: !SillyTime
  }
  deriving (Generic, Show)

data Weather = Weather
  { currentCondition :: !CurrentCondition,
    nearestArea :: !NearestArea,
    astronomy :: !Astronomy
  }
  deriving (Generic, Show)

getFirst :: Parser [a] -> Parser a
getFirst ls = do
  l <- ls
  case l of
    (a : _) -> return a
    [] -> fail "No current conditions"

instance FromJSON CurrentCondition where
  parseJSON = withObject "CurrentCondition" $ \v ->
    CurrentCondition <$> v .: "FeelsLikeF"
      <*> v .: "cloudcover"
      <*> v .: "humidity"
      <*> v .: "temp_F"
      <*> ( withArray
              "WeatherDescriptions"
              ( \vs -> do
                  concat <$> mapM (withObject "Description" (.: "value")) vs
              )
              =<< (v .: "weatherDesc")
          )
      <*> v .: "windspeedMiles"
      <*> v .: "winddir16Point"
      <*> ( withText
              "observationTime"
              ( \txt -> case Data.Text.unpack <$> Data.Text.words txt of
                  [a, b, c] -> return (SillyTime a c b)
                  _ -> fail $ printf "1 Failed to parse SillyTime [%s]" (Data.Text.unpack txt)
              )
              =<< (v .: "localObsDateTime")
          )

instance FromJSON NearestArea where
  parseJSON = withObject "NearestArea" $ \v ->
    fmap NearestArea $ do
      (areaNames :: [Object]) <- v .: "areaName"
      concat <$> mapM (.: "value") areaNames

parseAstronomy :: String -> Value -> Parser Astronomy
parseAstronomy date = withObject "Astronomy" $ \v ->
  Astronomy <$> (withText "sunrise" toSillyTime =<< (v .: "sunrise")) <*> (withText "sunset" toSillyTime =<< (v .: "sunset"))
  where
    toSillyTime str =
      case Data.Text.unpack <$> Data.Text.words str of
        [a, b] -> return $ SillyTime date b a
        _ -> fail $ printf "2 Failed to parse SillyTime [%s]" (Data.Text.unpack str)

instance FromJSON Weather where
  parseJSON = withObject "Weather" $ \v ->
    Weather
      <$> getFirst (v .: "current_condition")
      <*> getFirst (v .: "nearest_area")
      <*> ( withObject
              "Timeline"
              ( \v -> do
                  d <- v .: "date"
                  parseAstronomy d =<< getFirst (v .: "astronomy")
              )
              =<< getFirst (v .: "weather")
          )

conditionsIconDay :: [(String -> Bool, String)]
conditionsIconDay =
  [ ((== "overcast"), fc "#808080" "\xe312"),
    ((== "fair"), fc "a0a0a0" "🌑"),
    ((== "clear"), fc "#ddcf04" "\xe30d"),
    ((== "sunny"), fc "#ddcf04" "\xf05a8"),
    ((== "mostly clear"), fc "#00a3c4" "\xe30c"),
    ((== "mostly sunny"), fc "#ddcf04" "\xe30c"),
    ((== "partly sunny"), fc "#ddcf04" "\xe30c"),
    ((== "fair"), fc "#a0a0a0" "\xe30c"),
    ((== "cloudy"), fc "#a0a0a0" "\xf0590"),
    ((== "overcast"), fc "#808080" "\xf0590"),
    ((== "partly cloudy"), fc "#a0a0a0" "\xe302"),
    ((== "mostly cloudy"), fc "#808080" "\xe302"),
    ((== "considerable cloudiness"), fc "#a0a0a0" "\xe32e"),
    (("snow" `isInfixOf`), fc "#a0a0f0" "j")
  ]

conditionsIconNight :: [(String -> Bool, String)]
conditionsIconNight =
  [ ((== "clear"), fc "#00a3c4" "\xe32b"),
    ((== "sunny"), fc "#00a3c4" "\xe32b"),
    ((== "mostly clear"), fc "#00a3c4" "\xe37b"),
    ((== "mostly sunny"), fc "#00a3c4" "\xe37b"),
    ((== "partly sunny"), fc "#00a3c4" "\xe37b"),
    ((== "fair"), fc "#808080" "\xe379"),
    ((== "cloudy"), fc "#808080" "\xf0590"),
    ((== "overcast"), fc "#404040" "\xf0590"),
    ((== "partly cloudy"), fc "#a0a0a0" "\xf0f31"),
    ((== "mostly cloudy"), fc "#808080" "\xf0f31"),
    ((== "considerable cloudiness"), fc "\xf0590" "y")
  ]

handleWeather :: Weather -> String
handleWeather w = execWriter $ do
  tell $ lightGrey $ fn 3 $ areaName (nearestArea w)
  tell " "
  tell $
    lightGrey $
      fn 3 $
        case winddir (currentCondition w) of
          "NE" -> "\xf46c"
          "NNE" -> "\xeaa1"
          "ENE" -> "\xea9c"
          "SE" -> "\xf43e"
          "SSE" -> "\xea9a"
          "ESE" -> "\xea9c"
          "NW" -> "\xf45c"
          "NNW" -> "\xeaa1"
          "WNW" -> "\xea9b"
          "SW" -> "\xf424"
          "SSW" -> "\xea9a"
          "WSW" -> "\xea9b"
          "N" -> "\xeaa1"
          "S" -> "\xea9a"
          "W" -> "\xea9b"
          "E" -> "\xea9c"
          _ -> "?"

  tell " "
  tell $ lightGrey $ fn 3 $ windspeedMiles (currentCondition w) ++ "mph"
  tell "  "

  let conditions = if isNight then conditionsIconNight else conditionsIconDay
  tell $
    fn 5 $
      fromMaybe "?" $
        findMatch (map Data.Char.toLower $ weatherDesc (currentCondition w)) conditions

  tell "   "
  tell $ lightGrey $ fn 3 $ printf "%s°F" (tempF $ currentCondition w)
  where
    isNight =
      observationTime (currentCondition w) < sunrise (astronomy w)
        || observationTime (currentCondition w) > sunset (astronomy w)

    lightGrey = fc "#a0a0a0"

    fn :: Int -> String -> String
    fn = printf "<fn=%d>%s</fn>"

fc :: String -> String -> String
fc = printf "<fc=%s>%s</fc>"

findMatch :: a -> [(a -> Bool, b)] -> Maybe b
findMatch a ((f, b) : fs) | f a = Just b
findMatch a (_ : fs) = findMatch a fs
findMatch _ [] = Nothing

main :: IO ()
main = do
  (code, resp) <- curlGetString "https://wttr.in?format=j2" []
  when (code /= CurlOK) exitFailure

  case eitherDecode (Data.ByteString.Lazy.Char8.pack resp) of
    Left err -> hPutStrLn stderr $ printf "Failure to parse [%s]" err
    Right weather -> putStrLn $ handleWeather weather
