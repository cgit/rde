#!/bin/bash

frac="$1"

if [[ "$frac" == -* ]] ; then
  frac=${frac//-}
  t='sub'
elif [[ "$frac" == +* ]] ; then
  frac=${frac//+}
  t='add'
else
  t='abs'
fi

max="$(cat /sys/class/backlight/intel_backlight/max_brightness)"
to_set="$(bc <<< "$max * $frac")"
to_set=$(cut -d. -f1 <<< "$to_set") # Cut off the fractional part.

if [[ "$t" == 'sub' ]] ; then
  cur="$(cat /sys/class/backlight/intel_backlight/brightness)"
  to_set=$((cur - to_set))
elif [[ "$t" == 'add' ]] ; then
  cur="$(cat /sys/class/backlight/intel_backlight/brightness)"
  to_set=$((cur + to_set))
fi

if [ "$to_set" -gt "$max" ] ; then
  to_set="$max"
fi

if [ "$to_set" -lt 0 ] ; then
  to_set=0
fi

echo "$to_set" | tee /sys/class/backlight/intel_backlight/brightness
