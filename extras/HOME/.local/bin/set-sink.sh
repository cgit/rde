#!/bin/bash

if [[ -z "$ROFI" ]] ; then
  ROFI='rofi -dmenu'
fi

DMENU=($ROFI -i -theme-str '* {theme-color: #88ff88;}')

sinks="$(
  pactl list sinks | while read line ; do
    case "$line" in
      Sink\ \#*)
        name="${line//*#}" ;;
      Description:\ *)
        description="${line//*: }"
        echo "$description "'#'"$name"
        ;;
      *) ;;
    esac
  done)"

client_input="$(
  pactl list sink-inputs | while read line ; do
    case "$line" in
      application.name\ =*)
        app="${line//*= \"}"
        app="${app%%\"}"
        echo "$app: $media "'#'"$obj"
        ;;
      media.name\ =*)
        media="${line//*= \"}"
        media="${media%%\"}"
        ;;
      Sink\ Input\ \#*)
        obj="${line//*#}"
        ;;
    esac
  done
)"

echo "Client Input: $client_input"

if [[ "$(wc -l <<< "$client_input")" -gt 1 ]] ; then
  client_input="$("${DMENU[@]}" -p "Move Audio From" <<< "$client_input")"
fi

if [[ "$client_input" == "" ]] ; then
  exit 1
fi

input_sink=${client_input//*#}
input_sink_name=${client_input%% #*}

selected_sink=$("${DMENU[@]}" -p "Move '$input_sink_name' To" <<< "$sinks")
sink_num=${selected_sink//*#}

echo "Sinks: $sinks"

echo "pactl move-sink-input $input_sink $sink_num"
pactl move-sink-input "$input_sink" "$sink_num"
