#!/bin/bash

if [[ -z "$ROFI" ]] ; then
  ROFI='rofi -dmenu'
fi

if ( which rofi ) ; then
  menu=($ROFI -i -theme-str '* {theme-color: #ffa050;}' -p "Select Emoticon")
else
  menu=(dmenu -fn NotoSans:size=24 -i -nf "#ffff88" -sb "#ffff88" -p "Select Emoticon" -l 12 -dim 0.4)
fi


emoticon="$("${menu[@]}" < $HOME/.xmonad/emoticons.txt | sed 's#^[^-]*-\s*##')"

echo "$emoticon" | xclip -selection clipboard
