#!/bin/bash

if [ $# -lt 1 ]
then
        echo "No command?"
        exit
fi

function get-default-target() {
  local target_hint="spotify"
  while [[ "$1" == --* ]] ; do
    local arg="$1"
    case "$arg" in
      --target-hint=*)
        target_hint="${arg/--target-hint=/}"
        ;;
      *)
        echo "Bad Argument $1"
        exit 1
        ;;
    esac
    shift
  done
  
  # Some targets (spotifyd) don't have a stable dbus path.
  local targets="$( \
    dbus-send \
      --print-reply \
      --dest=org.freedesktop.DBus \
      /org/freedesktop/DBus org.freedesktop.DBus.ListNames \
    | sed 's#.*string \"\(.*\)"#\1#' \
    | grep MediaPlayer2)"
  
  # Prefer the target hint.
  target="$(echo "$targets" | grep -i "$target_hint" | head -n1)"
  if [ -z "$target" ] ; then
    # If no spotify, pick an arbitrary one
    target="$(echo "$targets" | head -n1)"
  fi
}

media_selection_file=$XDG_RUNTIME_DIR/rde/vars/MEDIA
if [ -f "$media_selection_file" ] ; then
  target=$(cat "$media_selection_file")
else
  get-default-target
fi

function mpris2_dbus_player_do {
  method="$1"
  shift

  dbus-send \
    --print-reply \
    --dest="$target" \
    /org/mpris/MediaPlayer2 \
    "org.mpris.MediaPlayer2.Player.$method" \
    "$@"
}

function mpris2_dbus_get_player_property {
  dbus-send \
    --print-reply \
    --dest="$target" \
    /org/mpris/MediaPlayer2 \
    org.freedesktop.DBus.Properties.Get \
    string:'org.mpris.MediaPlayer2.Player' "string:$1"
}

case $1 in
    "play")
        mpris2_dbus_player_do PlayPause
        ;;
    "seekf")
        mpris2_dbus_player_do Seek "int64: 12000000"
        ;;
    "seekb")
        mpris2_dbus_player_do Seek "int64: -3000000"
        ;;
    "next")
        mpris2_dbus_player_do Next
        ;;
    "pause")
        mpris2_dbus_player_do Pause
        ;;
    "justplay")
        mpris2_dbus_player_do Play
        ;;
    "prev")
        mpris2_dbus_player_do Previous
        ;;
    "getTitle")
        mpris2_dbus_get_player_property 'Metadata' | \
          egrep -A 1 "title" | \
          egrep -v "title" | \
          cut -b 44- | \
          cut -d '"' -f 1 | \
          egrep -v ^$
        ;;
    "getArtist")
        mpris2_dbus_get_player_property 'Metadata' | \
          -A 2 "artist" | \
          egrep -v "artist" | \
          egrep -v "array" | \
          cut -b 27- | \
          cut -d '"' -f 1 | \
          egrep -v ^$
        ;;
    "getAlbum")
        mpris2_dbus_get_player_property 'Metadata' | \
          egrep -A 2 "album" | \
          egrep -v "album" | \
          egrep -v "array" | \
          cut -b 44- | \
          cut -d '"' -f 1 | \
          egrep -v ^$
        ;;
    "getStatus")
        mpris2_dbus_get_player_property 'PlaybackStatus' | \
          grep 'string "[^"]*"' | \
          sed 's/.*"\(.*\)"[^"]*$/\1/'
        ;;
    *)
        echo "Unknown command: " $1
        ;;
esac
