#!/bin/bash

runtime_dir=$XDG_RUNTIME_DIR/rde/vars/
mkdir -p $runtime_dir

media_selection_file=$runtime_dir/MEDIA

raw_dbus_opts=$( \
  dbus-send \
    --print-reply \
    --dest=org.freedesktop.DBus \
    /org/freedesktop/DBus org.freedesktop.DBus.ListNames | \
  grep MediaPlayer2 | \
  sed 's#.*["]\(.*\)["]#\1#')

selection="$(echo -e "$raw_dbus_opts\ndefault" | \
  $ROFI -i -p "Control Media"  \
  -theme-str '* {theme-color: #88ff88;}')"

if [ ! -z "$selection" ] ; then
  if [ "$selection" == "default" ] ; then
    rm $media_selection_file
  else
    echo "$selection" > $media_selection_file
  fi
fi
