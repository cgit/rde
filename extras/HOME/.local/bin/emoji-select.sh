#!/bin/bash

if [[ -z "$ROFI" ]] ; then
  ROFI='rofi -dmenu'
fi

if ( which rofi ) ; then
  menu=($ROFI -i -p "Select Character" -theme-str '* {theme-color: #ffff88;}' -show run)
else
  menu=(dmenu -fn NotoSans:size=24 -i -nf "#ffff88" -sb "#ffff88" -p "Select Character" -l 12 -dim 0.4)
fi


selection="$(
  zcat $HOME/.xmonad/unicode.gz | sed 's@\([^;]\+\);\([^;]\+\).*@\1 \2 @g' |
    "${menu[@]}")"

emoji="${selection%% *}"

echo -n "$emoji" | xclip -selection clipboard
