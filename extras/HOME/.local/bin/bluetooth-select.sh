#!/bin/bash

run() {
  out=$("$@" 2>&1)
  if [ "$?" -ne 0 ] ; then
    gxmessage "$out"
  fi
}

disconnect() {
  echo "Disconnecting"
  run bluetoothctl -- disconnect
}

power_off() {
  echo "Power Off"
  run bluetoothctl -- power off
}

power_on() {
  echo "Power On"
  run bluetoothctl -- power on
}

scan_on() {
  echo "Scan On"
  run bluetoothctl -- scan on
}

scan_off() {
  echo "Scan Off"
  run bluetoothctl -- scan off
}

declare -A ops

ops["Disconnect"]=disconnect
ops["Power On"]=power_on
ops["Power Off"]=power_off
ops["Scan On"]=scan_on
ops["Scan Off"]=scan_off

ops_keys=""
for k in "${!ops[@]}" ; do
  ops_keys="$ops_keys\n$k"
done


if [[ -z "$ROFI" ]] ; then
  ROFI='rofi -dmenu'
fi

devices="$(bluetoothctl -- devices | sed 's#^Device ##')"
selection="$(
  echo -e "$devices\n$ops_keys" | $ROFI -i -p "Connect Bluetooth" \
    -theme-str '* {theme-color: #8888ff;}' \
    -dmenu)"

if [ -z "${selection}" ] ; then
  exit 1
fi

echo "selection=${selection}"

op="${ops[$selection]}"
if [[ -z "$op" ]] ; then
  macaddr="${selection%% *}"
  echo "macaddr=${macaddr}"
  run bluetoothctl -- connect "$macaddr"
else
  eval "$op"
fi

