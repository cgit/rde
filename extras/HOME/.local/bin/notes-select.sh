#!/bin/bash

mkdir -p "$HOME/.notes"
options="$(cd "$HOME/.notes" && find -type f -not -path '*/.*' | sed 's#^./##')"
selection="$(
  echo -e "$options" | $ROFI -i -p "Notes" \
    -kb-custom-1 "Alt+d" \
    -kb-custom-2 "Control+c" \
    -theme-str '* {theme-color: #f0f0a0;}' \
    )"

ec="$?"
if [[ "$ec" -eq 10 ]] ; then
  action="delete"
elif [[ "$ec" -eq 11 ]] ; then
  action="copy"
else
  action="edit"
fi

if [ -z "${selection}" ] ; then
  exit 1
fi

function do_edit {
  local path="$(readlink -fm "$1")"
  mkdir -p "$(dirname "$path")"
  alacritty --class floating-terminal -e vim "$path"

  if [[ "$path" == *"/shared/"* ]] ; then
    cd "$HOME/.notes/shared"

    git add "$path"
    git commit -m "Added $(echo "$path" | sed 's#'"$HOME"'#.notes/shared/#')"
  fi
}

fullnotepath="$HOME/.notes/${selection}"
echo "$action"
case "$action" in
  "copy")
    mkdir -p $(dirname "$fullnotepath")

    if [[ -e "$fullnotepath" ]] ; then
      cat "$fullnotepath" | sed -z '$ s/\n$//' | xclip -selection clipboard
    else
      do_edit "$fullnotepath"
    fi
    ;;
  "edit")
    do_edit "$fullnotepath"
    ;;
  "delete")
    echo "delete"
    rm "${fullnotepath}"
esac
