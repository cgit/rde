#!/bin/bash

MENU=(rofi -i -dmenu -sort -theme-str '* {theme-color: #ff88ff;}' -p "Library")

if (which zathura) ; then
  pdfviewer=zathura
elif (which evince) ; then
  pdfviewer=evince
else
  pdfviewer='xdg-open'
fi

if [ ! -e ~/Library ] ; then
  mkdir -p ~/Library
fi


cd ~/Library

file_with_title="$(find . -name '*.pdf' | while read file ; do
  echo "$file| $(echo "$file" | sed \
  's#\(^\|_\|\s\)\([a-z]\)#\1\U\2#g;s/\.[^.]*$//;s#^\(.*\)/\([^/]*\)$#\2 (\1)#' | tr '_' ' ')"
done)"

selected=$(echo "$file_with_title" | (while read file ; do
  echo "${file//*|}"
done) | "${MENU[@]}")

if [ ! -z "${selected}" ] ; then
  $pdfviewer "$(echo "$file_with_title" | grep "$selected" | sed 's/|.*//')"
fi
