#!/bin/bash

rofi=($ROFI -theme-str "* {theme-color: #88ffff;}")

if [[ "$1" == "-a" ]] ; then
  sinks="$(pactl list sinks | (while read line ; do
       case $line in
         Description:*)
           descr=${line//*: }
           ;;
         Sink\ \#*)
           if [ ! -z "$sink" ] ; then
             echo "$state $descr ($volume)|$sink"
           fi
           sink=${line//*#} ;;
         Volume:*)
           volume=$(sed 's/.* \([0-9]\+%\).*/\1/g' <<< "$line")
           ;;
         *State:*)
           state=${line//*: }
           if [[ "$state" == 'RUNNING' ]] ; then
             state='⏺'
           else
             state='  '
           fi ;;
       esac
    done
    echo "$state $descr ($volume)|$sink"))"
  sinks="$(grep "^⏺" <<< "$sinks" ; grep -v "^⏺" <<< "$sinks")"

  sink_inputs="$(pactl list sink-inputs | (while read line ; do
       case $line in
         *application.name\ =*)
           app="${line//*= \"}"
           app="${app%%\"}"
           ;;
         *media.name\ =*)
           media="${line//*= \"}"
           media="${media%%\"}"
           ;;
         *Volume:*)
           volume=$(sed 's/.* \([0-9]\+%\).*/\1/g' <<< "$line")
           ;;
         Sink\ Input\ \#*)
           if [ ! -z "$sink" ] ; then
             echo "⏺ $app: $media ($volume)|$sink"
           fi
           sink=${line//*#} ;;
       esac
    done
    echo "⏺ $app: $media ($volume)|$sink"))"

    selection=$(
        (echo "$sinks" ; echo '' ; echo "$sink_inputs") | \
          cut -d'|' -f1 | \
          "${rofi[@]}" -p "Set Volume For")

    if [[ -z "$selection" ]] ; then
      echo "Cancelled" >&2
      exit 0
    fi

    value=$(grep "$selection" <<< "$sinks")
    command="set-sink-volume"
    if [[ -z "$value" ]] ; then
      command="set-sink-input-volume"
      value=$(grep "$selection" <<< "$sink_inputs")
      if [[ -z "$value" ]] ; then
        echo "Invalid Selection" >&2
        exit 1
      fi
    fi
    echo "Setting $value"
    value=${value//*|}
else
  command="set-sink-volume"
  value="@DEFAULT_SINK@"
fi

volume=$(echo '10%
 20%
 30%
 40%
 50%
 60%
 70%
 80%
 90%
 100%
 110%
 120%
 130%' | "${rofi[@]}" -p 'Set Volume To')

echo pactl "$command" "$value" "$volume"
pactl "$command" "$value" $volume
