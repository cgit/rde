module Rahm.Desktop.XMobarLog.PendingBuffer
  ( addStringToPendingBuffer,
    setPendingBuffer,
    clearPendingBuffer,
    getPendingBuffer,
    pushPendingBuffer,
    pushAddPendingBuffer,
  )
where

import Rahm.Desktop.Common
import Data.Default (Default (..))
import XMonad (X)
import qualified XMonad as X
  ( ExtensionClass (initialValue),
    MonadReader (ask),
    XConf (config),
    XConfig (logHook),
  )
import qualified XMonad.Util.ExtensibleState as XS
  ( get,
    modify,
    put,
  )

-- The pending buffer keeps track of pending characters. This is useful for when
-- inputing Wml language constructs. Helps to keep the user from being too lost
-- wheen it comes to keeping track of keystrokes.

newtype PendingBuffer = PendingBuffer {unPendingBuffer :: [Char]}

instance Default PendingBuffer where
  def = PendingBuffer []

instance X.ExtensionClass PendingBuffer where
  initialValue = def

addStringToPendingBuffer :: [Char] -> X ()
addStringToPendingBuffer str = do
  XS.modify $ \(PendingBuffer cs) ->
    PendingBuffer (cs ++ str)

  X.logHook . X.config =<< X.ask

setPendingBuffer :: [Char] -> X ()
setPendingBuffer cs = do
  XS.put $ PendingBuffer cs
  X.logHook . X.config =<< X.ask

clearPendingBuffer :: X ()
clearPendingBuffer = do
  XS.put (def :: PendingBuffer)
  X.logHook . X.config =<< X.ask

getPendingBuffer :: X [Char]
getPendingBuffer = unPendingBuffer <$> XS.get

pushPendingBuffer :: (Xish x) => String -> x a -> x a
pushPendingBuffer newPendingBuffer fn = do
  saved <- liftFromX getPendingBuffer
  liftFromX $ setPendingBuffer newPendingBuffer
  fn <* liftFromX (setPendingBuffer saved)

pushAddPendingBuffer :: (Xish x) => String -> x a -> x a
pushAddPendingBuffer toAdd fn = do
  saved <- liftFromX getPendingBuffer
  liftFromX $ setPendingBuffer (saved ++ toAdd)
  fn <* liftFromX (setPendingBuffer saved)
