module Rahm.Desktop.PassMenu (runPassMenu) where

import Control.Monad (void)
import XMonad (X)
import XMonad.Util.Run (safeSpawn)

runPassMenu :: X ()
runPassMenu =
  void $
    safeSpawn
      "rofi-pass"
      [ "-p",
        "Password ",
        "-theme-str",
        "* {theme-color: #f54245;}"
      ]
