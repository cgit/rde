-- Module to enable redescribing layouts. Unlike LayoutModifiers though, this
-- class is aware of the underlying type as it may need to access some internals
-- to generate the new description.
module Rahm.Desktop.Layout.Redescribe
  ( Describer (..),
    Redescribe (..),
  )
where

import qualified Rahm.Desktop.StackSet as W
  ( Workspace (Workspace),
  )
import XMonad
  ( LayoutClass (description, handleMessage, runLayout),
    Typeable,
  )

-- Type-class to modify the description of a layout.
class Describer m l where
  -- Returns the new description from the given description modifier, the layout
  -- and the existing description.
  newDescription :: m -> l a -> String -> String

-- With a DescriptionModifier,
data Redescribe m l a = Redescribe m (l a)
  deriving (Show, Read)

-- Delegates to the underlying Layout, except for the description
instance
  (Typeable m, Show m, Describer m l, LayoutClass l a) =>
  LayoutClass (Redescribe m l) a
  where
  runLayout (W.Workspace t (Redescribe m l) a) rect = do
    (rects, maybeNewLayout) <- runLayout (W.Workspace t l a) rect
    return (rects, fmap (Redescribe m) maybeNewLayout)

  handleMessage (Redescribe m l) a = do
    maybeNewLayout <- handleMessage l a
    return (Redescribe m <$> maybeNewLayout)

  description (Redescribe m l) = newDescription m l (description l)
