{- Swap window with the master, but save it. -}
module Rahm.Desktop.SwapMaster (swapMaster) where

import Control.Monad (void)
import Control.Monad.State (gets)
import Control.Monad.Trans (lift)
import Control.Monad.Trans.Maybe (MaybeT (..))
import Data.Map (Map)
import qualified Data.Map as Map
import Rahm.Desktop.Common (runMaybeT_)
import qualified Rahm.Desktop.StackSet as W
  ( currentTag,
    focusMaster,
    masterWindow,
    peek,
    swapWindows,
    tag,
  )
import XMonad
  ( ExtensionClass (..),
    Window,
    X (..),
    windows,
    windowset,
  )
import qualified XMonad.Util.ExtensibleState as XS (get, modify, put)

newtype LastWindow = LastWindow
  { lastWindows :: Map String Window
  }
  deriving (Show, Read)

instance ExtensionClass LastWindow where
  initialValue = LastWindow mempty

swapMaster :: X ()
swapMaster =
  runMaybeT_ $ do
    ss <- gets windowset
    let ct = W.currentTag ss

    (focused, master) <-
      hoist $ do
        a <- W.peek ss
        b <- W.masterWindow ss
        return (a, b)

    lift $ do
      st <- lastWindows <$> XS.get
      windows . W.swapWindows $
        case focused == master of
          True
            | (Just lw) <- Map.lookup ct st ->
              [(focused, lw)]
          False -> [(master, focused)]
          _ -> []

      XS.modify $ mlw (Map.insert (W.currentTag ss) master)
      windows W.focusMaster
  where
    mlw fn (LastWindow l) = LastWindow (fn l)
    hoist = MaybeT . return
