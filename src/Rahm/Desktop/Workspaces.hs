-- Common ways to select workspaces
module Rahm.Desktop.Workspaces
  ( getPopulatedWorkspaces,
    next,
    prev,
    lastWorkspaceId,
    firstWorkspaceId,
    windowsInCurrentWorkspace,
    getHorizontallyOrderedScreens,
    accompaningWorkspace,
    adjacentWorkspaceNotVisible,
    adjacentWorkspace,
    viewAdjacent,
    viewAdjacentTo,
    adjacentScreen,
    withScreen,
    workspaceWithWindow,
    getScreensOnSamePlane,
    getScreensOnDifferentPlane,
    WorkspaceState (..),
  )
where

import Control.Arrow (Arrow ((&&&)))
import Control.Monad.Trans (lift)
import Control.Monad.Trans.Maybe (MaybeT (MaybeT))
import Data.Char (isUpper, toLower, toUpper, chr, ord, isDigit)
import Data.List (find, sort, sortBy, sortOn, (\\))
import Data.List.Safe ((!!))
import Data.Maybe (fromMaybe, mapMaybe)
import Debug.Trace
import Rahm.Desktop.Common (getCurrentWorkspace, gotoWorkspace, runMaybeT_)
import Rahm.Desktop.Logger
import qualified Rahm.Desktop.StackSet as W
import Text.Printf (printf)
import XMonad
  ( Rectangle (Rectangle),
    ScreenDetail (SD),
    ScreenId,
    Window,
    WindowSet,
    WorkspaceId,
    X,
    windows,
    withWindowSet,
  )
import Prelude hiding ((!!))

newtype Selector = Selector (forall a. (a -> Bool) -> [a] -> Maybe a)

data WorkspaceState = Current | Hidden | Visible
  deriving (Ord, Eq, Enum)

-- Returns all the workspaces that are either visible, current or Hidden but
-- have windows and that workspace's state.
--
-- In other words, filters out workspaces that have no windows and are not
-- visible.
--
-- This function will sort the result by the workspace tag.
getPopulatedWorkspaces ::
  W.StackSet String l a sid sd -> [(WorkspaceState, W.Workspace String l a)]
getPopulatedWorkspaces (W.StackSet (W.Screen cur _ _) vis hi _) =
  filter ((/= "*") . W.tag . snd) $
    sortOn (W.tag . snd) $
      mapMaybe (\w@(W.Workspace _ _ s) -> fmap (const (Hidden, w)) s) hi
        ++ map (\(W.Screen w _ _) -> (Visible, w)) vis
        ++ [(Current, cur)]

next :: Selector
next = Selector $ \f l -> select f l l
  where
    select f (x : y : xs) _ | f x = Just y
    select f [x] (y : _) | f x = Just y
    select f (x : xs) orig = select f xs orig
    select f _ _ = Nothing

prev :: Selector
prev = Selector $ \f l ->
  let (Selector fn) = next in fn f (reverse l)

lastWorkspaceId :: X WorkspaceId
lastWorkspaceId =
  W.tag . snd . last <$> withWindowSet (return . getPopulatedWorkspaces)

firstWorkspaceId :: X WorkspaceId
firstWorkspaceId =
  W.tag . snd . head <$> withWindowSet (return . getPopulatedWorkspaces)

windowsInCurrentWorkspace :: X [Window]
windowsInCurrentWorkspace = withWindowSet $
  \(W.StackSet (W.Screen (W.Workspace _ _ s) _ _) _ _ _) -> do
    return $ W.integrate' s

getHorizontallyOrderedScreens ::
  W.StackSet wid l a ScreenId ScreenDetail ->
  [(Bool, W.Screen wid l a ScreenId ScreenDetail)]
-- ^ Returns a list of screens ordered from leftmost to rightmost.
getHorizontallyOrderedScreens windowSet =
  flip sortBy screens $ \sc1 sc2 ->
    let (SD (Rectangle x1 _ _ _)) = W.screenDetail (snd sc1)
        (SD (Rectangle x2 _ _ _)) = W.screenDetail (snd sc2)
     in x1 `compare` x2
  where
    screens = (True, W.current windowSet) : map (False,) (W.visible windowSet)

getVerticallyOrderedScreens ::
  W.StackSet wid l a ScreenId ScreenDetail ->
  [(Bool, W.Screen wid l a ScreenId ScreenDetail)]
-- ^ Returns a list of screens ordered from top to bottom
getVerticallyOrderedScreens windowSet =
  flip sortBy screens $ \sc1 sc2 ->
    let (SD (Rectangle _ y1 _ _)) = W.screenDetail (snd sc1)
        (SD (Rectangle _ y2 _ _)) = W.screenDetail (snd sc2)
     in y1 `compare` y2
  where
    screens = (True, W.current windowSet) : map (False,) (W.visible windowSet)

-- | Returns screens which are horizontally ordered, but are on the same "plane"
-- as the current screen. A screen is considered on the same "plane" if it's
-- middle point is vertically oriented within the vertical boundaries of the
-- current screen.
getScreensOnSamePlane ::
  W.StackSet wid l a ScreenId ScreenDetail ->
  [(Bool, W.Screen wid l a ScreenId ScreenDetail)]
getScreensOnSamePlane ss =
  filter matchesYCenter $ getHorizontallyOrderedScreens ss
  where
    yCenter
      | (SD (Rectangle _ y _ h)) <- W.screenDetail . W.current $ ss =
          (y + fromIntegral h) `div` 2
    matchesYCenter (_, W.screenDetail -> (SD (Rectangle _ y _ h))) =
      y < yCenter && y + fromIntegral h > yCenter

-- | Returns screens which are vertically ordered, but are on a different plane
-- from the current screen.
getScreensOnDifferentPlane ::
  W.StackSet wid l a ScreenId ScreenDetail ->
  [(Bool, W.Screen wid l a ScreenId ScreenDetail)]
getScreensOnDifferentPlane ss =
  filter (not . matchesScreen . getYCenter . snd) $ getVerticallyOrderedScreens ss
  where
    getYCenter (W.screenDetail -> SD (Rectangle _ y _ h)) =
      y + (fromIntegral h `div` 2)
    matchesScreen yCenter
      | (SD (Rectangle _ y _ h)) <- W.screenDetail (W.current ss) =
          yCenter < y + fromIntegral h && yCenter > y

accompaningWorkspace :: WorkspaceId -> WorkspaceId
accompaningWorkspace [s]
  | isDigit s = show (fl (ord s - ord '0'))
  where
    fl x | even x = x + 1
    fl x = x - 1
accompaningWorkspace [s] =
  return $
    if isUpper s
      then toLower s
      else toUpper s
accompaningWorkspace s = s

adjacentWorkspaceNotVisible :: Selector -> WorkspaceId -> X WorkspaceId
adjacentWorkspaceNotVisible (Selector selector) from =
  withWindowSet $ \ss ->
    let tags =
          sort $
            W.tag . snd
              <$> filter
                (\x -> fst x /= Visible)
                ( getPopulatedWorkspaces ss
                )
     in return $ fromMaybe from $ selector (== from) tags

adjacentWorkspace :: Selector -> WorkspaceId -> X WorkspaceId
adjacentWorkspace (Selector selector) from =
  withWindowSet $ \ss ->
    let tags = sort $ W.tag . snd <$> getPopulatedWorkspaces ss
     in return $ fromMaybe from $ selector (== from) tags

viewAdjacent :: Selector -> X ()
viewAdjacent sel =
  gotoWorkspace =<< (adjacentWorkspaceNotVisible sel =<< getCurrentWorkspace)

viewAdjacentTo :: X (Maybe WorkspaceId) -> Selector -> X ()
viewAdjacentTo wsM (Selector sel) = runMaybeT_ $ do
  lift $ logs Debug "viewAdjacentTo"
  tag <- MaybeT wsM
  lift $ logs Debug "from: %s" tag
  ws <- MaybeT $ withWindowSet $ \ws ->
    let vis = map (W.tag . W.workspace) (W.screens ws)
        allW = sort $ map (W.tag . snd) (getPopulatedWorkspaces ws)
        final = allW \\ (vis \\ [tag])
     in return $ sel (== tag) final
  lift $ logs Debug "to: %s" ws
  lift $ windows $ W.switchWorkspaces tag ws

adjacentScreen :: Selector -> X WorkspaceId
adjacentScreen (Selector f) = do
  (screens, current) <-
    withWindowSet $ return . (getHorizontallyOrderedScreens &&& W.current)

  return $ W.tag $ W.workspace $ maybe current snd (f fst screens)

withScreen :: (WorkspaceId -> WindowSet -> WindowSet) -> Int -> X ()
withScreen fn n = do
  windows $ \windowSet ->
    case map snd (getHorizontallyOrderedScreens windowSet) !! n of
      Nothing -> windowSet
      Just screen -> fn (W.tag $ W.workspace screen) windowSet

workspaceWithWindow :: Window -> X (Maybe WorkspaceId)
workspaceWithWindow wid = withWindowSet $ \(W.StackSet c v h _) ->
  return $
    W.tag
      <$> find
        (\(W.Workspace _ _ stack) -> wid `elem` W.integrate' stack)
        (map W.workspace (c : v) ++ h)
