{-# LANGUAGE OverloadedStrings #-}

module Rahm.Desktop.Hash (quickHash) where

import qualified Crypto.Hash.SHA1 as SHA1 (hash)
import qualified Data.ByteString as BS (unpack)
import qualified Data.ByteString.Char8 as BC (pack)
import Numeric (showHex)

quickHash :: String -> String
quickHash str =
  concatMap (`showHex` "") $ BS.unpack (SHA1.hash $ BC.pack str)
