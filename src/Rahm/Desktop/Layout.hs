module Rahm.Desktop.Layout
  ( myLayout,
    myLayoutList,
    nLayouts,
  )
where

import Rahm.Desktop.Layout.ConsistentMosaic
  ( MosaicWrap (..),
    expandPositionAlt,
    shrinkPositionAlt,
  )
import Rahm.Desktop.Layout.Explode (explodeable)
import Rahm.Desktop.Layout.Flip (flippable)
import Rahm.Desktop.Layout.Hole (hole)
import Rahm.Desktop.Layout.List
  ( layoutList,
    layoutListLength,
    nil,
    (|:),
  )
import Rahm.Desktop.Layout.PinWindow (PinWindowLayout (PinWindowLayout), pinnable)
import Rahm.Desktop.Layout.Pop (poppable)
import Rahm.Desktop.Layout.Redescribe (Describer (..), Redescribe (..))
import Rahm.Desktop.Layout.ReinterpretMessage (DoReinterpret (..), ReinterpretMessage (..))
import Rahm.Desktop.Layout.Rotate (rotateable)
import XMonad
  ( IncMasterN (..),
    Resize (..),
    SomeMessage (..),
    Tall (..),
    Window,
    fromMessage,
  )
import XMonad.Hooks.ManageDocks (avoidStruts)
import XMonad.Hooks.ScreenCorners (screenCornerLayoutHook)
import XMonad.Layout.Fullscreen (fullscreenFull)
import XMonad.Layout.LayoutModifier (ModifiedLayout (..))
import XMonad.Layout.MosaicAlt
  ( MosaicAlt (..),
  )
import XMonad.Layout.Spacing (Border (..), spacingRaw)
import XMonad.Layout.Spiral (spiral)

myLayout =
  screenCornerLayoutHook $
    fullscreenFull $
      hole $
        pinnable $
          avoidStruts myLayoutList

mySpacing = spacingRaw True (Border 5 5 5 5) True (Border 5 5 5 5) True

mods =
  poppable . explodeable . mySpacing . flippable . rotateable

myLayoutList =
  layoutList $
    mods (reinterpretIncMaster $ spiral (6 / 7))
      |: mods (MosaicWrap $ modifyMosaic (MosaicAlt mempty :: MosaicAlt Window))
      |: mods (Redescribe UsingTall (Tall 1 (3 / 100) (1 / 2)))
      |: nil

nLayouts :: Int
nLayouts = layoutListLength myLayoutList

-- Mosaic doesn't have the concept of a "Master Space", so reinterpret messages
-- intended to modify the master space and instead have those messages expand
-- and shrink the current window.
--
-- "ForMosaic" is an instance of the Symbol kind. This is some neat type-system
-- hacking one can do in Haskell.
instance DoReinterpret "ForMosaic" where
  -- IncMaster message
  reinterpretMessage _ (fromMessage -> Just (IncMasterN n)) = do
    Just . SomeMessage
      <$> ( if n > 0
              then expandPositionAlt
              else shrinkPositionAlt
          )

  -- ResizeMaster message
  reinterpretMessage _ (fromMessage -> Just m) = do
    Just . SomeMessage
      <$> ( case m of
              Expand -> expandPositionAlt
              Shrink -> shrinkPositionAlt
          )

  -- Messages that don't match the above, just leave it unmodified.
  reinterpretMessage _ m = return (Just m)

instance DoReinterpret "IncMasterToResizeMaster" where
  reinterpretMessage _ (fromMessage -> Just (IncMasterN n)) =
    return $
      Just $
        if n > 0
          then SomeMessage Expand
          else SomeMessage Shrink
  reinterpretMessage _ m = return (Just m)

modifyMosaic :: l a -> ModifiedLayout (ReinterpretMessage "ForMosaic") l a
modifyMosaic = ModifiedLayout ReinterpretMessage

reinterpretIncMaster ::
  l a -> ModifiedLayout (ReinterpretMessage "IncMasterToResizeMaster") l a
reinterpretIncMaster = ModifiedLayout ReinterpretMessage

data UsingTall = UsingTall deriving (Read, Show)

instance Describer UsingTall Tall where
  newDescription _ (Tall mast _ _) _ = "Tall(" ++ show mast ++ ")"
