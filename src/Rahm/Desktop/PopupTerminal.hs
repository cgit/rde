module Rahm.Desktop.PopupTerminal where

import XMonad
import qualified XMonad.Util.ExtensibleState as XS
import Data.Monoid
import Control.Monad.Trans
import qualified XMonad.StackSet as W
import Data.Foldable (forM_)
import XMonad.Util.SpawnOnce (spawnOnce)

newtype PopupTerminalState = PopupTerminalState
 {
   popupTerminalWindow :: Maybe Window
 } deriving (Show, Read)

instance ExtensionClass PopupTerminalState where
  initialValue = PopupTerminalState Nothing
  extensionType = PersistentExtension

getPopupTerminalWindow :: X (Maybe Window)
getPopupTerminalWindow = XS.gets popupTerminalWindow

movePopupToCurrentWorkspace :: X ()
movePopupToCurrentWorkspace = do
  mWin <- getPopupTerminalWindow
  forM_ mWin $ \win -> do
    windows $ \ws ->
     W.focusWindow win $
      W.shiftWin (W.tag (W.workspace (W.current ws))) win ws

movePopupToHiddenWorkspace :: X ()
movePopupToHiddenWorkspace = do
  mWin <- getPopupTerminalWindow
  forM_ mWin $ \win -> windows $ \ws -> W.shiftWin "*" win ws

updatePopupTerminalHook :: ManageHook
updatePopupTerminalHook = Query $ do
  win <- ask
  lift $ XS.put $ PopupTerminalState (Just win)
  return (Endo id)
