import Control.Monad.Reader
  ( MonadReader (ask),
    ReaderT (runReaderT),
    liftM2, MonadIO (liftIO),
  )
import Data.Char (toUpper)
import Data.List (isInfixOf, isPrefixOf)
import Data.Monoid (All (All), Endo (Endo))
import qualified Rahm.Desktop.BorderColors
import Rahm.Desktop.Common
  ( Location (Location),
    getCurrentWorkspace,
  )
import Rahm.Desktop.DMenu (menuCommandString)
import Rahm.Desktop.History (historyHook)
import Rahm.Desktop.Hooks.WindowChange (withStackChangeHook)
import Rahm.Desktop.Keys (applyKeys)
import Rahm.Desktop.Layout (myLayout)
import Rahm.Desktop.Logger (LogLevel (Debug, Info, Trace), logs)
import Rahm.Desktop.Marking (Mark, markAllLocations)
import Rahm.Desktop.PopupTerminal
import Rahm.Desktop.RebindKeys (WindowHook, remapHook)
import qualified Rahm.Desktop.StackSet as W
  ( RationalRect (..),
    allWindows,
    float,
  )
import Rahm.Desktop.XMobarLog (spawnXMobar, xMobarLogHook)
import System.Directory (getHomeDirectory)
import System.Environment (setEnv)
import System.FilePath ((</>))
import XMonad
  ( Default (def),
    ManageHook,
    Query (Query),
    X,
    XConfig
      ( borderWidth,
        clickJustFocuses,
        focusFollowsMouse,
        focusedBorderColor,
        handleEventHook,
        keys,
        layoutHook,
        logHook,
        manageHook,
        modMask,
        normalBorderColor,
        startupHook,
        terminal,
        workspaces
      ),
    XState (dragging),
    appName,
    className,
    composeAll,
    doF,
    doFloat,
    doIgnore,
    doShift,
    floatLocation,
    liftX,
    mod4Mask,
    modify,
    mouseDrag,
    refresh,
    spawn,
    title,
    withWindowSet,
    (-->),
    (=?),
  )
import qualified XMonad as X (xmonad)
import XMonad.Hooks.DynamicProperty (dynamicTitle)
import XMonad.Hooks.EwmhDesktops (ewmh)
import XMonad.Hooks.ManageDocks (docks)
import XMonad.Hooks.ManageHelpers (doFullFloat, isFullscreen)
import XMonad.Hooks.ScreenCorners
import XMonad.Layout.Fullscreen (fullscreenEventHook)
import XMonad.Util.SpawnOnce (spawnOnce)

main = do
  logHook <- xMobarLogHook
  putStrLn "Welcome To RDE!"

  -- Execute some commands.
  homeDir <- getHomeDirectory
  let fp = homeDir </> ".xmonad" </> "startup"

  setEnv "SUDO_ASKPASS" "/usr/bin/ssh-askpass"
  setEnv "ROFI" menuCommandString

  xmobar <- spawnXMobar

  (=<<) X.xmonad
    $ applyKeys
    $ withStackChangeHook
      ( mconcat
          [ historyHook,
            Rahm.Desktop.BorderColors.stackChangeHook
          ]
      )
    $ ewmh
    $ docks
    $ def
      { terminal = "alacritty",
        modMask = mod4Mask,
        borderWidth = 2,
        keys = const mempty,
        focusedBorderColor = "#ff6c00",
        normalBorderColor = "#404040",
        layoutHook = myLayout,
        startupHook = do
          spawn fp
          spawnOnce "alacritty --class popup-terminal"
          addScreenCorner SCUpperLeft (liftIO $ putStrLn "Hello, World!"),

        manageHook =
          composeAll
            [ isFullscreen --> doFullFloat,
              doLogWindow,
              className =? "Tilda" --> doFloat,
              className =? "yakuake" --> doFloat,
              className =? "MPlayer" --> doFloat,
              className =? "Xfce4-notifyd" --> doIgnore,
              className =? "popup-terminal" --> doShift "*" <> updatePopupTerminalHook,
              className =? "spotify" --> doShift "s",
              className =? "floating-terminal" --> doCenterFloat,
              title =? "Event Tester" --> doFloat,
              title =? "Notes" --> doCenterFloat,
              title =? "xmessage" --> doCenterFloat,
              title =? "gxmessage" --> doCenterFloat,
              fmap (isInfixOf "wlroots") title --> doCenterFloat,
              title =? "Volume Control" --> doCenterFloat,
              className =? "mpv" --> doFloat,
              className =? "gnubby_ssh_prompt" --> doFloat,
              shouldChromeFloat --> doFloat
            ],
        -- This config uses dynamic workspaces, but I have to seed XMonad
        -- with something. However, this configuration only supports 36
        -- monitors on boot. If you need more than 15 monitors, you'll have to
        -- configure those ones after starting XMonad.
        workspaces =
          map return (['w', 'r', 'j', 's', 't'] ++ ['0' .. '9']),
        handleEventHook =
          composeAll
            [ screenCornerEventHook,
              traceLogHook,
              fullscreenEventHook,
              remapHook,
              dynamicTitle
                ( composeAll
                    [ title =? "Spotify" --> doMarkWindow "s"
                    ]
                )
            ],
        focusFollowsMouse = False,
        clickJustFocuses = False,
        logHook = logHook xmobar
      }
  where
    traceLogHook e = do
      logs Trace "Event found: %s" (show e)
      (return (All True) :: X All)

-- Should the chrome window float? Returns false if the window's title starts
-- with "New Tab" because this usually means a new window was launched. If the
-- window name does not start with "New Tab", it is likely the tab was torn out
-- of the browser. These windows should float to avoid screwing with tiling.
shouldChromeFloat :: Query Bool
shouldChromeFloat = do
  className <- className
  title <- title
  return $
    className == "Google-chrome" && not ("New Tab" `isPrefixOf` title)

changeHook :: Location -> Location -> X ()
changeHook l1 l2 =
  logs Info "Change %s -> %s" (show l1) (show l2)

doLogWindow :: ManageHook
doLogWindow = do
  t <- title
  c <- className
  a <- appName
  liftX $ logs Debug "New Window {title: \"%s\", class: \"%s\", appName: \"%s\"}" t c a
  return (Endo id)

doMarkWindow :: Mark -> ManageHook
doMarkWindow m =
  ask
    >>= ( \w ->
            liftX
              ( do
                  ws <- getCurrentWorkspace
                  markAllLocations m [Location ws (Just w)]
              )
              >> return (Endo id)
        )

doCenterFloat :: ManageHook
doCenterFloat =
  ask >>= \w -> doF . W.float w . centerRect . snd =<< liftX (floatLocation w)

centerRect :: W.RationalRect -> W.RationalRect
centerRect (W.RationalRect x y w h) = W.RationalRect ((1 - w) / 2) ((1 - h) / 2) w h

windowHooks :: WindowHook -> XConfig l -> XConfig l
windowHooks (Query readerT) config = do
  config
    { startupHook = do
        withWindowSet $ mapM_ (runReaderT readerT) . W.allWindows
        startupHook config,
      manageHook = mappend (Query readerT >> return (Endo id)) (manageHook config)
    }
